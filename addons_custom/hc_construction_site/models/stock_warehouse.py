from odoo import models, fields, api, _, SUPERUSER_ID


class StockWarehouse(models.Model):
    _inherit = 'stock.warehouse'

    x_construction_site_id = fields.Many2one('construction.site', 'Construction Site', domain=[('active', '=', True)], required=True,
                                             index=True)

    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        domain = []
        if name:
            domain = [('name', operator, name)]

        access_right = []
        # Check user root and user view all
        if self.env.uid == SUPERUSER_ID or self.env.user.is_system or self.env.user.x_view_all_site or self._context.get('request_view'):
            return self._search(domain + args + access_right, limit=limit, access_rights_uid=name_get_uid)

        site_ids = self.env.user.x_construction_site_ids
        if site_ids:
            access_right = [('x_construction_site_id', 'in', site_ids.ids)]

        return self._search(domain + args + access_right, limit=limit, access_rights_uid=name_get_uid)

    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
        if self.env.uid == SUPERUSER_ID or self.env.user.is_system or self.env.user.x_view_all_site or self._context.get('request_view'):
            domain = domain
        else:
            site_access_ids = self.env.user.x_construction_site_ids
            site_ids = site_access_ids.ids
            domain_warehouse = []
            domain_warehouse.append('x_construction_site_id')
            if len(site_ids) == 1:
                domain_warehouse.append('=')
                listToStr = ' '.join(map(str, site_ids))
                ite_id = int(listToStr)
                domain_warehouse.append(ite_id)
            else:
                domain_warehouse.append('in')
                domain_warehouse.append(site_ids)
            domain.append(domain_warehouse)
        return super(StockWarehouse, self).search_read(domain, fields, offset, limit, order)
