from odoo import models, fields, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError


class ConstructionSite(models.Model):
    _name = 'construction.site'
    _description = 'Construction Management'
    _order = 'create_date desc'

    name = fields.Char('Name', required=True)
    code = fields.Char('Code', index=True, required=True)
    address = fields.Char('Address')
    manager_id = fields.Many2one('res.users', 'Manager', index=True)
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.company)
    active = fields.Boolean('Active', default=True)

    _sql_constraints = [
        ('code_uniq', 'unique (code)', 'The code of the construction site must be unique per company !')
    ]

    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        domain = []
        if name:
            domain = [('name', operator, name)]

        access_right = []
        # Check user root and user view all
        if self.env.uid == SUPERUSER_ID or self.env.user.is_system or self.env.user.x_view_all_site:
            return self._search(domain + args + access_right, limit=limit, access_rights_uid=name_get_uid)

        site_ids = self.env.user.x_construction_site_ids
        if site_ids:
            access_right = [('id', 'in', site_ids.ids)]

        return self._search(domain + args + access_right, limit=limit, access_rights_uid=name_get_uid)

    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
        if self.env.uid == SUPERUSER_ID or self.env.user.is_system or self.env.user.x_view_all_site:
            domain = domain
        else:
            site_access_ids = self.env.user.x_construction_site_ids
            site_ids = site_access_ids.ids
            domain_site = []
            domain_site.append('id')
            if len(site_ids) == 1:
                domain_site.append('=')
                listToStr = ' '.join(map(str, site_ids))
                ite_id = int(listToStr)
                domain_site.append(ite_id)
            else:
                domain_site.append('in')
                domain_site.append(site_ids)
            domain.append(domain_site)
        return super(ConstructionSite, self).search_read(domain, fields, offset, limit, order)

    @api.model
    def create(self, vals):
        res = super(ConstructionSite, self).create(vals)
        res._create_missing_warehouse(res.name, res.code)
        return res

    def write(self, vals):
        res = super(ConstructionSite, self).write(vals)
        for rc in self:
            warehouse_id = self.env['stock.warehouse'].sudo().search([('x_construction_site_id', '=', rc.id)])
            if not warehouse_id:
                rc._create_missing_warehouse(rc.name, rc.code)

        return res

    def _create_missing_warehouse(self, name, code):
        wh_vals = {
            'name': 'Kho ' + name,
            'code': 'K' + code,
            'x_construction_site_id': self.id,
            'company_id': self.env.company.id
        }
        self.env['stock.warehouse'].create(wh_vals)
