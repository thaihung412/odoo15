from odoo import models, fields, api, _


class ResUser(models.Model):
    _inherit = 'res.users'

    x_construction_site_ids = fields.Many2many('construction.site', 'res_user_construction_site_rel', 'user_id', 'site_id',
                                               'Construction Site', domain=[('active', '=', True)])
    x_view_all_site = fields.Boolean('View all construction site', default=False)
