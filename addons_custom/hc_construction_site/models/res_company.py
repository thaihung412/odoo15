from odoo import models, fields, api, _


class ResCompany(models.Model):
    _inherit = 'res.company'

    x_manager_id = fields.Many2one('res.users', 'Giám đốc')
