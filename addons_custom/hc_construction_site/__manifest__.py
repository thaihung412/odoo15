# -*- coding: utf-8 -*-
{
    'name': "Construction Site Management",
    'summary': """
    This application allows construction site management by warehouse
    """,

    'description': """
        This application allows construction site management by warehouse 
    """,
    'author': "Công nguyễn",
    'website': "https://www.facebook.com/cn.dinh/",
    'category': 'Stock',
    'version': '0.1',
    'depends': ['base', 'stock'],
    'data': [
        'security/ir.model.access.csv',
        'views/construction_site.xml',
        'views/res_user_view.xml',
        'views/res_company_view.xml',
        'views/stock_warehouse_view.xml',
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
