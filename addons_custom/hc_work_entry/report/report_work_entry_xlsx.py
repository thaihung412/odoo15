import io
import base64

from odoo import models
from odoo.exceptions import ValidationError

from datetime import datetime
from PIL import Image


class ReportJobXLSX(models.AbstractModel):
    _name = 'report.hc_work_entry.report_work_entry_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, lines):
        try:
            for obj in lines:
                format_title = workbook.add_format(
                    {'font_size': 20, 'valign': 'middle', 'bold': False, 'left': True, 'right': True, 'text_wrap': True,
                     'align': 'center'
                     })
                format_1 = workbook.add_format(
                    {'font_size': 12, 'valign': 'middle', 'bold': True, 'left': True, 'right': True, 'text_wrap': True,
                     'align': 'center'
                     })
                format_2 = workbook.add_format(
                    {'font_size': 12, 'valign': 'middle', 'bold': False, 'left': True, 'right': True, 'text_wrap': True,
                     'align': 'left',
                     })

                img_logo = base64.b64decode(self.env.company.logo)
                image_logo = io.BytesIO(img_logo)
                bound_width_height = (200, 100)
                image_data_logo = self.get_resized_image_data(image_logo, bound_width_height)
                im_logo = Image.open(image_data_logo)
                im_logo.seek(0)
                sheet = workbook.add_worksheet('Báo cáo chấm công')
                sheet.set_column(0, 0, 10)
                sheet.set_column(1, 1, 15)
                sheet.set_column(2, 2, 80)
                sheet.set_column(3, 3, 60)
                sheet.set_column(4, 13, 20)

                sheet.merge_range('A1:B3', '')
                sheet.insert_image('A1:B3', 'mylogoimage.png', {'image_data': image_data_logo})

                sheet.merge_range('A4:F4', 'Báo cáo chấm công', format_title)
                sheet.write('A6', 'STT', format_1)
                sheet.write('B6', 'Ngày', format_1)
                sheet.write('C6', 'Tên công nhân', format_1)
                sheet.write('D6', 'Thuộc đội', format_1)
                sheet.write('E6', 'Số công', format_1)
                sheet.write('F6', 'Công trường', format_1)
                row_count = 7
                stt = 1


                for line in obj.line_ids:
                    sheet.write('A' + str(row_count), stt, format_1)
                    sheet.write('B' + str(row_count), datetime.strftime(line.date, '%d/%m/%Y'), format_1)
                    sheet.write('C' + str(row_count), line.employee_id.name, format_2)
                    sheet.write('D' + str(row_count), line.team_id.name, format_2)
                    sheet.write('E' + str(row_count), line.ca, format_2)
                    sheet.write('F' + str(row_count), line.construction_site_id.name, format_2)
                    row_count += 1
                    stt += 1

                border_format = workbook.add_format({
                    'border': 1,
                    'align': 'left',
                    'font_size': 10
                })

                sheet.conditional_format(0, 0, row_count - 2, 5,
                                         {'type': 'no_blanks', 'format': border_format})
                sheet.conditional_format(0, 0, row_count - 2, 5,
                                         {'type': 'blanks', 'format': border_format})
        except Exception as e:
            raise ValidationError(e)

    def get_resized_image_data(self, file_path, bound_width_height):
        # get the image and resize it
        im = Image.open(file_path)
        im.thumbnail(bound_width_height, Image.ANTIALIAS)  # ANTIALIAS is important if shrinking

        # stuff the image data into a bytestream that excel can read
        im_bytes = io.BytesIO()
        im.save(im_bytes, format='PNG')
        return im_bytes
