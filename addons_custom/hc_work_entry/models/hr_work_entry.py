from odoo import models, fields, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError


class HrWorkEntry(models.Model):
    _name = 'hr.work.entry.hc'
    _description = 'HR Work Entry'
    _order = 'create_date desc'

    name = fields.Char(default='Chấm công')
    team_id = fields.Many2one('hr.work.entry.team.hc', string='Team')
    employee_id = fields.Many2one('hr.employee', string='Employee')
    construction_site_id = fields.Many2one('construction.site', string='Contruction site')
    date = fields.Date(string='Date')
    ca = fields.Selection([
        ('1', '1'),
        ('0.5', '0.5')
    ], tracking=True)




