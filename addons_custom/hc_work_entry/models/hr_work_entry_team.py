from odoo import models, fields, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError


class HrWorkEntryTeam(models.Model):
    _name = 'hr.work.entry.team.hc'
    _description = 'HR Work Entry'
    _order = 'create_date desc'

    name = fields.Char('Name', required=True)
    manager_id = fields.Many2one('res.users', 'Manager', index=True)
    construction_site_id = fields.Many2one('construction.site', string='Công trường')