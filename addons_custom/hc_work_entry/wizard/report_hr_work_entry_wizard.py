from odoo import models, fields, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError, UserError


class ReportJobWizard(models.TransientModel):
    _name = 'report.work.entry.wizard'

    from_date = fields.Date('From date', required=True)
    to_date = fields.Date('To date', required=True)
    construction_site_ids = fields.Many2many('construction.site', string='Construction site')
    team_id = fields.Many2many('hr.work.entry.team.hc', string='Tổ đội')
    employee_id = fields.Many2many('hr.employee', string='Employee')
    line_ids = fields.One2many('report.work.entry.wizard.line', 'report_id', 'Line')

    def action_print_excel(self):
        try:
            report_name = 'hc_work_entry.report_work_entry_xlsx'
            action = self.env['ir.actions.report'].search([
                ('model', '=', self._name),
                ('report_name', '=', report_name),
                ('report_type', '=', 'xlsx'),
            ], limit=1)
            if not action:
                raise UserError(_('Report Template not found'))
            context = dict(self.env.context)
            return action.with_context(context).report_action(self)
        except Exception as e:
            raise ValidationError(e)

    def action_generate_total(self):
        if self.line_ids:
            self.line_ids.unlink()
        obj = self
        domain = [('date', '>=', obj.from_date), ('date', '<=', obj.to_date)]
        if obj.construction_site_ids:
            domain = [('date', '>=', obj.from_date), ('date', '<=', obj.to_date),
                      ('construction_site_id', 'in', obj.construction_site_ids.ids)]
        if obj.employee_id:
            domain = [('date', '>=', obj.from_date), ('date', '<=', obj.to_date),
                      ('employee_id', 'in', obj.employee_id.ids)]
        if obj.team_id:
            domain = [('date', '>=', obj.from_date), ('date', '<=', obj.to_date),
                      ('team_id', 'in', obj.team_id.ids)]
        if obj.employee_id and obj.construction_site_ids:
            domain = [('date', '>=', obj.from_date), ('date', '<=', obj.to_date),
                      ('employee_id', 'in', obj.employee_id.ids),
                      ('construction_site_id', 'in', obj.construction_site_ids.ids)]
        if obj.employee_id and obj.team_id:
            domain = [('date', '>=', obj.from_date), ('date', '<=', obj.to_date),
                      ('employee_id', 'in', obj.employee_id.ids),
                      ('team_id', 'in', obj.team_id.ids)]
        if obj.construction_site_ids and obj.team_id:
            domain = [('date', '>=', obj.from_date), ('date', '<=', obj.to_date),
                      ('construction_site_id', 'in', obj.construction_site_ids.ids),
                      ('team_id', 'in', obj.team_id.ids)]
        if obj.construction_site_ids and obj.team_id and obj.employee_id:
            domain = [('date', '>=', obj.from_date), ('date', '<=', obj.to_date),
                      ('construction_site_id', 'in', obj.construction_site_ids.ids),
                      ('employee_id', 'in', obj.employee_id.ids),
                      ('team_id', 'in', obj.team_id.ids)]
        # else:
        #     if self.env.user.x_view_all_site or self.env.user.is_system:
        #         domain = [('date', '>=', obj.from_date), ('date', '<=', obj.to_date)]
        #     else:
        #         domain = [('date', '>=', obj.from_date), ('date', '<=', obj.to_date),
        #                   ('construction_site_id', 'in', self.env.user.x_construction_site_ids)]
        transfer_line_ids = self.env['hr.work.entry.hc'].search(domain)
        values = []
        for line in transfer_line_ids:
            vals = {
                'report_id': self.id,
                'date': line.date,
                'employee_id': line.employee_id.id,
                'team_id': line.team_id.id,
                'ca': line.ca,
                'construction_site_id': line.construction_site_id.id

            }
            values.append((0, 0, vals))
        self.line_ids = values


class ReportJobWizardLine(models.TransientModel):
    _name = 'report.work.entry.wizard.line'

    report_id = fields.Many2one('report.work.entry.wizard', 'Report ID')
    date = fields.Date(string='Ngày')
    employee_id = fields.Many2one('hr.employee', string='Công nhân')
    team_id = fields.Many2one('hr.work.entry.team.hc', string='Đội')
    ca = fields.Float(string='Số công')
    construction_site_id = fields.Many2one('construction.site', string='Công trường')


