# -*- coding: utf-8 -*-
{
    'name': "Hr Work Entry",
    'summary': """
    HR Work Entry
    """,

    'description': """
        This application allows construction site management by warehouse 
    """,
    'author': "",
    'website': "",
    'category': 'Human Resources',
    'version': '0.1',
    'depends': ['base', 'hr', 'hc_construction_site', 'report_xlsx'],
    'data': [
        'security/ir.model.access.csv',
        'security/rule_security.xml',
        'views/hr_work_entry_view.xml',
        'views/hr_work_entry_team.xml',
        'wizard/report_work_entry_wizard.xml',
        'report/report.xml'
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
