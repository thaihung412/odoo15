# -*- coding: utf-8 -*-

{
    'name': 'Notification',
    'version': '1.0',
    'summary': 'Notification Website',
    'description': "",
    'depends': ['base', 'base_setup', 'bus', 'web_tour', 'mail', 'htv_web_notify'],
    'data': [

        'views/mail_templates.xml',

    ],
    'qweb': [

        'static/src/xml/systray.xml',

    ],
}
