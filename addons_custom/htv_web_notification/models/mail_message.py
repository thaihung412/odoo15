# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
import re

from binascii import Error as binascii_error
from collections import defaultdict
from operator import itemgetter
from odoo.http import request
from datetime import datetime

from odoo import _, api, fields, models, modules, tools
from odoo.exceptions import UserError, AccessError
from odoo.osv import expression
from odoo.tools import groupby

_logger = logging.getLogger(__name__)
_image_dataurl = re.compile(r'(data:image/[a-z]+?);base64,([a-z0-9+/\n]{3,}=*)\n*([\'"])(?: data-filename="([^"]*)")?',
                            re.I)


class Message(models.Model):
    """ Messages model: system notification (replacing res.log notifications),
        comments (OpenChatter discussion) and incoming emails. """
    _inherit = 'mail.message'

    message_type = fields.Selection(selection_add=[('system_notification', 'System NotifiÏcation')])
    res_form_id = fields.Integer(string="Ref Form View")
    active = fields.Boolean(string="Active", default=True)
    status = fields.Selection([('seen', 'Seen'), ('unseen', 'Unseen')], default='unseen')
    icon = fields.Char(string="Icon", help='Icon vector for system notification')

    def update_status_message(self, *args, **kwargs):
        self.write({
            'status': 'seen'
        })
        firebase_notification = self.env['firebase.notification'].sudo().search([('message_id','=',self.id)])
        if len(firebase_notification) > 0:
            firebase_notification.mark_seen()
        self.env['bus.bus'].sudo().sendone(
            (self._cr.dbname, 'res.partner', self.author_id.id),
            {'type': 'notification_updated', 'notification_seen': True})

    # thêm api để có thể call api
    @api.model
    def message_read_all(self):
        message_ids = self.env['mail.message'].sudo().search(
            [('active', '=', False), ('status', '=', 'unseen'), ('partner_ids', 'in', self.env.user.partner_id.ids)])
        for message in message_ids:
            message.update_status_message()
        self.env['bus.bus'].sudo().sendone(
            (self._cr.dbname, 'res.partner', self.author_id.id),
            {'type': 'notification_updated', 'notification_seen': True})

    def _push_system_notification(self, author_id, recipients, subject_notification, model, res_id, icon='fa-user',
                                  body='', record_name='',
                                  timestamp=None, res_form_id=False):
        """ Push system notification to recipients. Content's notification will be hidden on message log.
            Record has used for widget notification.

                   :param int author_id: author_id: object res.partner
                   :param list recipients: list recipients:  list id object res.partner
                   :param str subject_notification: subject's notification
                   :param str model: Model
                   :param int res_id: record of the model
                   :param str icon
                   :param str body
                   :param str record_name
                   :param datetime timestamp: time expected sent notification
                   :param int res_form_id: form_view_id
               """
        partner_id = self.env['res.partner'].sudo().browse([author_id])
        subtype_id = self.env.ref('mail.mt_note').id
        message_ids = []
        for rc in recipients:
            values = {
                'canned_response_ids': [],
                'author_id': author_id,
                'email_from': f'"{partner_id.name}" <{partner_id.email}>',
                'model': model,
                'create_date': timestamp if timestamp else datetime.now(),
                'res_id': res_id,
                'body': body,
                'subject': subject_notification,
                'message_type': 'system_notification',
                'partner_ids': [[6, 0, [rc]]],
                'active': False,
                'parent_id': False,
                'subtype_id': subtype_id,
                'status': 'unseen',
                'add_sign': True,
                'record_name': record_name,
                'icon': icon,
                'res_form_id': res_form_id,
                'attachment_ids': []}

            message_id = self.env['mail.message'].sudo().create(values)
            message_ids.append(message_id)

        self._push_notification(author_id=author_id, recipients=recipients, subject_notification=subject_notification)
        return

    def _push_notification(self, author_id, recipients, subject_notification):
        """ Push system notification to recipients. Content's notification will be hidden on message log.
            Record has used for widget notification.
                   :param int author_id: author_id: object res.partner
                   :param list recipients: list recipients:  list id object res.partner
                   :param str subject_notification: subject's notification
               """
        if author_id:
            for at in recipients:
                self.env['bus.bus'].sudo().sendmany([[
                    (self._cr.dbname, 'res.partner', at),
                    {'type': 'notification_updated', 'notification_unseen': True}]])
                user = self.env['res.users'].sudo().search([('partner_id', '=', at)], limit=1)
                if user.id:
                    user.sudo().notify_info(message=subject_notification)

        return
