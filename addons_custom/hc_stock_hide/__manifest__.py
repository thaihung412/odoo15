# -*- coding: utf-8 -*-
{
    'name': "Stock Hide",
    'summary': """
    This application allows to hide unnecessary information in the stock
    """,

    'description': """
        This application allows to hide unnecessary information in the stock
    """,
    'author': "Công nguyễn",
    'website': "https://www.facebook.com/cn.dinh/",
    'category': 'Stock',
    'version': '0.1',
    'depends': ['base', 'stock', 'product'],
    'data': [
        'views/menu_hide.xml',
        'views/stock_hide_view.xml',
        'views/product_hide_view.xml',
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
