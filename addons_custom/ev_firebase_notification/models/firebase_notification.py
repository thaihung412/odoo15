# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from pyfcm import FCMNotification
from datetime import datetime, timedelta as td
import logging

STATE_SELECTOR = [('new', 'New'), ('sent', 'sent'), ('fail', 'Fail'), ('seen', 'Seen')]
SOUND_SELECTOR = [('Default', 'Default')]
DEFAULT_FIREBASE_SERVER_KEY = 'AAAApG3MTmM:APA91bE3RJcqoZCyWyI0CSvnZNNzaBAMCFimehclzOSxRJb4cxQIZZJ5PF17PKgOmw78YAAAUqfZGiObPp4vpvuagNxY2K5lFo8fkMSYy6qyB-lPKRSzc0RKBkxbVXyz4fA2PgIcwiQl'
_logger = logging.getLogger(__name__)


class FirebaseNotification(models.Model):
    _name = 'firebase.notification'
    _description = "Firebase notification"

    user_id = fields.Many2one('res.users', string='User')
    subject = fields.Char(string='Subject')
    message = fields.Text(string='Message')
    sound = fields.Selection(SOUND_SELECTOR, string='Sound', default='Default')
    state = fields.Selection(STATE_SELECTOR, default='new', string='Status')
    data = fields.Text(string="Data")
    android_channel_id = fields.Char(string='Android channel id')
    message_id = fields.Many2one('mail.message', string="Message", ondelete='cascade')

    def action_send(self, model, res_id, x_type):
        firebase_token = []
        devices = self.env['mobile.device.token'].search([('partner_id', '=', self.user_id.sudo().partner_id.id)])
        create_date = self.create_date + td(hours=7)
        str_time = str(create_date).split(' ')
        s_time = str_time[1].split('.')
        time = s_time[0] if len(s_time) > 1 else s_time[0]
        day = create_date.day if create_date.day >= 10 else f'0{create_date.day}'
        month = create_date.month if create_date.month >= 10 else f'0{create_date.month}'
        str_create_date = f'{day}-{month}-{create_date.year} {time}'
        data_message = {
            'subject': self.subject,
            'message': self.message,
            'model': model,
            'res_id': res_id,
            'type': x_type,
            'datetime': str_create_date
        }
        for device in devices:
            firebase_token.append(device.token)
        if len(firebase_token):
            self.__send(firebase_tokens=firebase_token, data_message=data_message)

    def mark_seen(self):
        self.state = 'seen'

    def mark_fail(self):
        self.state = 'fail'

    def mark_sent(self):
        self.state = 'sent'

    def __send(self, firebase_tokens, data_message):
        try:
            api_key = self.__get_api_key()
            if not api_key:
                message = _('You do not have the firebase server key,'
                            '\nplease configure in the system parameters with the key is firebase_server_key')
                raise ValidationError(message)
            badge = self.count_badge()
            push_service = FCMNotification(api_key=api_key)
            push_service.notify_multiple_devices(registration_ids=firebase_tokens,
                                                 message_title=self.subject,
                                                 message_body=self.message,
                                                 sound=self.sound,
                                                 badge=badge,
                                                 android_channel_id=self.android_channel_id,
                                                 data_message=data_message)
            self.mark_sent()
        except Exception as e:
            self.mark_fail()

    def __get_api_key(self):
        config = self.env['ir.config_parameter'].search([('key', '=', 'firebase_server_key')])
        if config:
            return config.value
        return DEFAULT_FIREBASE_SERVER_KEY

    def count_badge(self):
        query = """SELECT act.id as message_id,(select id from ir_model where model = act.model) as model_id, act.res_id as res_id, 
                        act.model as model, act.subject as subject, act.status as status, act.icon as icon, act.create_date as create_date
                        from mail_message as act
                        inner join mail_message_res_partner_rel as mmrpr on mmrpr.mail_message_id = act.id
                        where act.message_type = 'system_notification' and model != 'documents.document' and
                        mmrpr.res_partner_id = %(partner_id)s and act.status = 'unseen'
                        order by act.status desc, act.create_date desc"""
        self.env.cr.execute(query, {
            'partner_id': self.user_id.partner_id.id,
        })
        total_record = self.env.cr.rowcount
        return total_record
