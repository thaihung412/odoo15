# -*- coding: utf-8 -*-
{
    'name': "Firebase notification",

    'summary': """Quản lý thông báo sử dụng firebase""",

    'description': """
        Cấu hình firebase api key trong cấu hình thông số hệ thống với key là: firebase_api_key
    """,

    'author': "IziSolution",
    'website': "http://www.izisolution.vn",
    'category': 'Notification',
    'version': '0.1',
    'depends': ['htv_web_notification', 'bus', 'mail'],
    'data': [
        'security/ir.model.access.csv',
        'data/data.xml',
        'views/firebase_notification.xml',
    ],
    'demo': [],
    'external_dependencies': {
        'python': [
            'pyfcm',
        ],
    },
}
