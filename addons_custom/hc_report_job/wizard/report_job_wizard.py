from odoo import models, fields, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError, UserError


class ReportJobWizard(models.TransientModel):
    _name = 'report.job.wizard'

    from_date = fields.Date('From date', required=True)
    to_date = fields.Date('To date', required=True)
    construction_site_ids = fields.Many2many('construction.site', string='Construction site')

    def action_print_excel(self):
        try:
            report_name = 'hc_report_job.report_job_xlsx'
            action = self.env['ir.actions.report'].search([
                ('model', '=', self._name),
                ('report_name', '=', report_name),
                ('report_type', '=', 'xlsx'),
            ], limit=1)
            if not action:
                raise UserError(_('Report Template not found'))
            context = dict(self.env.context)
            return action.with_context(context).report_action(self)
        except Exception as e:
            raise ValidationError(e)
