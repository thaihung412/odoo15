from datetime import datetime

from odoo import models, fields, api, _, SUPERUSER_ID


class ReportJob(models.Model):
    _name = 'report.job'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Report Job'
    _order = 'create_date desc'

    name = fields.Char('Name', default=lambda self: _('New'))
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id, required=True)
    date = fields.Datetime('Date', default=fields.Datetime.now, required=True)
    construction_site_id = fields.Many2one('construction.site', 'Construction Site', domain=[('active', '=', True)], required=True)
    content = fields.Text('Content', required=True)
    image = fields.Image('Image')
    state = fields.Selection([('draft', 'Nháp'), ('done', 'Hoàn thành')], 'Trạng thái', default='draft')

    def action_done(self):
        self.ensure_one()
        if self.state != 'draft':
            return True
        self._create_send_notification()
        self.date = datetime.now()
        self.state = 'done'

    def _create_send_notification(self):
        msg = 'Báo cáo: {} được tạo bởi {} '.format(self.name, self.create_uid.name)
        manager_id = self.env.user.company_id.x_manager_id
        channel = self.env['mail.channel'].channel_get([manager_id.partner_id.id])
        channel_id = self.env['mail.channel'].browse(channel["id"])
        channel_id.message_post(
            body=(msg),
            message_type='comment',
            subtype_xmlid='mail.mt_comment',
        )

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('report.job') or _('New')
        return super(ReportJob, self).create(vals)

    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        domain = []
        if name:
            domain = [('name', operator, name)]

        access_right = []
        # Check user root and user view all
        if self.env.uid == SUPERUSER_ID or self.env.user.is_system:
            return self._search(domain + args + access_right, limit=limit, access_rights_uid=name_get_uid)

        domain_request = ['create_uid', '=', self.env.user.id]

        return self._search(domain + args + domain_request, limit=limit, access_rights_uid=name_get_uid)

    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
        if self.env.uid == SUPERUSER_ID or self.env.user.is_system:
            domain = domain
        else:
            domain_request = ['create_uid', '=', self.env.user.id]
            domain.append(domain_request)
        return super(ReportJob, self).search_read(domain, fields, offset, limit, order)
