# -*- coding: utf-8 -*-
{
    'name': "Report Job",
    'summary': """
    This application allows Report Job
    """,

    'description': """
        This application allows Report Job
    """,
    'author': "Công nguyễn",
    'website': "https://www.facebook.com/cn.dinh/",
    'category': 'Report',
    'version': '0.1',
    'depends': ['base', 'stock', 'hc_work_entry'],
    'data': [
        'data/ir_sequence_data.xml',
        'security/ir.model.access.csv',
        'security/hc_report_job_security.xml',
        'views/report_job_view.xml',
        'wizard/report_job_wizard.xml',
        'report/report.xml',
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
