# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError


class StockTransferLine(models.Model):
    _name = 'stock.transfer.line'

    transfer_id = fields.Many2one('stock.transfer', 'Stock transfer')
    product_id = fields.Many2one('product.product', 'Product', domain=[('type', 'in', ['product', 'consu'])])
    product_uom_category_id = fields.Many2one('uom.category', related='product_id.uom_id.category_id')
    uom_id = fields.Many2one('uom.uom', 'Uom', domain="[('category_id', '=', product_uom_category_id)]")
    quantity = fields.Float('Quantity', 'Quantity request', default=0)
    in_quantity = fields.Float('In Quantity', default=0)
    out_quantity = fields.Float('Out Quantity', default=0)
    available_qty = fields.Float('Available Quantity', default=0, readonly=True)
    price = fields.Float('Price', default=0)
    price_outgoing = fields.Float('Price outgoing', compute='_compute_price_outgoing', store=True)
    price_total = fields.Float('Price Total', compute='_compute_price_total')
    is_available = fields.Boolean('Check duplicate', default=False)

    state = fields.Selection([('not_available', 'Not Available'), ('available', 'Available')], 'State')

    @api.depends('product_id')
    def _compute_price_outgoing(self):
        for rc in self:
            line_before = self.env['stock.transfer.line'].search([
                ('transfer_id.type', '=', 'incoming'),
                ('product_id', '=', rc.product_id.id),
            ], limit=1, order="id desc")
            if line_before:
                rc.price_outgoing = line_before.price
            else:
                rc.price_outgoing = 0

    @api.onchange('product_id')
    def onchange_product_id(self):
        product = self.product_id.with_context(lang=self.env.user.lang)
        self.uom_id = product.uom_id
        return {'domain': {'uom_id': [('category_id', '=', product.uom_id.category_id.id)]}}

    @api.depends('out_quantity', 'price')
    def _compute_price_total(self):
        for rc in self:
            qty = rc.in_quantity if rc.transfer_id.type == 'incoming' else rc.out_quantity
            rc.price_total = qty * rc.price_outgoing

    def _create_stock_moves(self, picking):
        moves = self.env['stock.move']
        for line in self:
            vals = line._prepare_stock_moves(picking)
            move_id = moves.create(vals)

    def _prepare_stock_moves(self, picking):
        """ Prepare the stock moves data for one order line. This function returns a list of
        dictionary ready to be used in stock.move's create()
        """
        self.ensure_one()
        res = []
        if self.product_id.type not in ['product', 'consu']:
            return res
        vals = {
            'name': self.transfer_id.partner_id.ref or '',
            'product_id': self.product_id.id,
            'product_uom': self.uom_id.id,
            'product_uom_qty': self.in_quantity if self.transfer_id.type == 'incoming' else self.out_quantity,
            'date': self.transfer_id.date,
            'date_deadline': self.transfer_id.date,
            'location_id': picking.location_id.id,
            'location_dest_id': picking.location_dest_id.id,
            'picking_id': picking.id,
            'x_transfer_line_id': self.id,
            'state': 'draft',
            'company_id': self.transfer_id.company_id.id,
            'picking_type_id': picking.picking_type_id.id,
            'origin': self.transfer_id.name,
            'route_ids': (picking.picking_type_id.warehouse_id and [
                (6, 0, [x.id for x in picking.picking_type_id.warehouse_id.route_ids])] or []),
            'warehouse_id': picking.picking_type_id.warehouse_id.id,
        }
        res.append(vals)
        return res

    def unlink(self):
        for rc in self:
            if rc.transfer_id.state != 'draft':
                raise UserError(_('you can only delete when the transfer slip is in draft state'))
        return super(StockTransferLine, self).unlink()

    @api.model
    def create(self, vals_list):
        transfer_id = self.env['stock.transfer'].search([('id', '=', vals_list.get('transfer_id'))], limit=1)
        if transfer_id and transfer_id.state != 'draft':
            raise UserError(_('you can only delete when the transfer slip is in draft state'))

        return super(StockTransferLine, self).create(vals_list)
