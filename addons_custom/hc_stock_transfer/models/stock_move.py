# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class StockMove(models.Model):
    _inherit = 'stock.move'

    x_transfer_line_id = fields.Many2one('stock.transfer.line', 'Transfer line id')
