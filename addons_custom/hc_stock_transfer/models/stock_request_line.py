# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class StockRequestLine(models.Model):
    _name = 'stock.request.line'
    _description = 'Stock Request Detail'

    request_id = fields.Many2one('stock.request', string='Request')
    product_id = fields.Many2one('product.product', string='Product', domain=[('type', 'in', ['product', 'consu'])], required=True)
    uom_id = fields.Many2one('uom.uom', string='Uom')
    qty = fields.Float('Quantity Request', digits='Product Unit of Measure')
    available_qty = fields.Float('Available Quantity', default=0, readonly=True)
    price_unit = fields.Float('Price Unit', digits=(0,0))
    is_available = fields.Boolean('Check duplicate', default=False)
    state = fields.Selection([('not_available', 'Not Available'), ('available', 'Available')], 'State')

    @api.onchange('product_id')
    def onchange_product_id(self):
        product = self.product_id.with_context(lang=self.env.user.lang)
        self.uom_id = product.uom_id
        return {'domain': {'uom_id': [('category_id', '=', product.uom_id.category_id.id)]}}
