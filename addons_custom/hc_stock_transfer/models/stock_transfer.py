# -*- coding: utf-8 -*-
from odoo import models, fields, api, _, SUPERUSER_ID
from odoo.tools.float_utils import float_compare, float_is_zero, float_round
from odoo.exceptions import UserError, AccessError, except_orm
from odoo.osv import osv
import base64

from datetime import datetime, timedelta


class StockTransfer(models.Model):
    _name = 'stock.transfer'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'create_date desc'
    _description = 'Stock transfer'

    name = fields.Char('Transfer Code', default=lambda self: _('New'), tracking=True)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    warehouse_id = fields.Many2one('stock.warehouse', 'Warehouse Outgoing', tracking=True)
    warehouse_dest_id = fields.Many2one('stock.warehouse', 'Warehouse Incoming', tracking=True)
    partner_id = fields.Many2one('res.partner', 'Partner', tracking=True)
    type = fields.Selection([('incoming', 'Incoming'), ('outgoing', 'Outgoing')], 'Type', required=True)
    is_internal = fields.Boolean('Is transfer internal', default=False)
    date = fields.Datetime('Date', default=fields.Datetime.now)
    in_date = fields.Datetime('In Date')
    out_date = fields.Datetime('Out Date')
    state = fields.Selection([('draft', 'Draft'), ('confirmed', 'Confirmed'), ('ready', 'Ready'),
                              ('transfer', 'Transfer'), ('done', 'Done'), ('cancel', 'Cancel')],
                             'State', default="draft", tracking=True)
    out_transfer_count = fields.Integer(string='Out Transfer', compute='_compute_out_transfer')
    in_transfer_count = fields.Integer(string='In Transfer', compute='_compute_in_transfer')
    out_picking_id = fields.Many2one('stock.picking', 'Stock picking from')
    in_picking_id = fields.Many2one('stock.picking', 'Stock picking to')
    # request
    origin = fields.Char('Origin')
    line_ids = fields.One2many('stock.transfer.line', 'transfer_id', 'Transfer Line')

    def _compute_out_transfer(self):
        for order in self:
            order.out_transfer_count = len(order.out_picking_id)

    def _compute_in_transfer(self):
        for rc in self:
            rc.in_transfer_count = len(rc.in_picking_id)

    def action_view_out_transfer(self):
        action = self.env['ir.actions.act_window']._for_xml_id('stock.action_picking_tree_all')
        if self.out_picking_id:
            action['domain'] = [('id', '=', self.out_picking_id.id)]
            action['views'] = [(self.env.ref('stock.view_picking_form').id, 'form')]
            action['res_id'] = self.out_picking_id.id
        return action

    def action_view_in_transfer(self):
        action = self.env['ir.actions.act_window']._for_xml_id('stock.action_picking_tree_all')
        if self.in_picking_id:
            action['domain'] = [('id', '=', self.in_picking_id.id)]
            action['views'] = [(self.env.ref('stock.view_picking_form').id, 'form')]
            action['res_id'] = self.in_picking_id.id
        return action

    def action_check_available(self):
        self.check_qty()
        self.delete_duplicate()
        check_available = self._check_available_quantity()
        if check_available:
            self.state = 'confirmed'
        else:
            self.action_confirm()

    def action_confirm(self):
        if self.state not in ('draft', 'confirmed') or not self.line_ids:
            return True
        self.check_qty()
        self.delete_duplicate()
        if self.type == 'incoming':
            if self.is_internal:
                picking_type_id = self.warehouse_dest_id.int_type_id
                location_id = self.warehouse_dest_id.x_location_transfer_id
                location_dest_id = self.warehouse_dest_id.lot_stock_id
            else:
                picking_type_id = self.warehouse_dest_id.in_type_id
                location_id = self.env['stock.location'].search([('usage', '=', 'supplier')], limit=1)
                location_dest_id = self.warehouse_dest_id.lot_stock_id
        else:
            if self.is_internal:
                picking_type_id = self.warehouse_id.int_type_id
                location_id = self.warehouse_id.lot_stock_id
                location_dest_id = self.warehouse_id.x_location_transfer_id
            else:
                picking_type_id = self.warehouse_id.out_type_id
                location_id = self.warehouse_id.lot_stock_id
                location_dest_id = self.warehouse_id.x_location_production_id

        picking_id = self._create_picking(picking_type_id.id, location_id.id, location_dest_id.id)
        picking_id.action_confirm()
        if self.type == 'incoming':
            self.in_picking_id = picking_id
        else:
            self.out_picking_id = picking_id

        self.state = 'ready'
        if not self.is_internal:
            self.action_done()

    def action_done(self):
        picking_id = self.in_picking_id if self.type == 'incoming' else self.out_picking_id
        self._check_transfer_outgoing()
        if self.type == 'outgoing':
            check_available = self._check_available_quantity()
            if check_available:
                raise UserError(_('Please adjust the output quantity to match the available quantity'))
        for line in self.line_ids:
            move_ids = self.env['stock.move'].search([('picking_id', '=', picking_id.id), ('product_id', '=', line.product_id.id)])
            qty = line.in_quantity if self.type == 'incoming' else line.out_quantity
            for move in move_ids:
                if qty > 0:
                    move.quantity_done = qty
        picking_id.button_validate()
        if self.type == 'incoming':
            # picking_id.action_assign()
            self.in_date = datetime.now()
        else:
            self.out_date = datetime.now()

        self.state = 'done'

    def _create_picking(self, picking_type_id, location_id, location_dest_id):
        stock_picking = self.env['stock.picking']
        picking = False
        for transfer in self:
            if any([ptype in ['product', 'consu'] for ptype in transfer.line_ids.mapped('product_id.type')]):
                res = transfer._prepare_picking(picking_type_id, location_dest_id, location_id)
                picking = stock_picking.create(res)
                moves = transfer.line_ids._create_stock_moves(picking)
        return picking

    @api.model
    def _prepare_picking(self, picking_type_id, location_dest_id, location_id):
        return {
            'picking_type_id': picking_type_id,
            'date': self.date,
            'origin': self.name,
            'location_dest_id': location_dest_id,
            'location_id': location_id,
            'partner_id': self.partner_id.id,
            'company_id': self.company_id.id
        }

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('stock.transfer') or _('New')

        return super(StockTransfer, self).create(vals)

    def unlink(self):
        for rc in self:
            if rc.state == 'draft':
                if rc.is_internal:
                    raise UserError(_('Thông báo! Bạn không thể xóa phiếu điểu chuyển này'))
            else:
                raise UserError(_('Thông báo! Bạn chỉ có thể xóa ở trạng thái nháp'))
        return super(StockTransfer, self).unlink()

    def action_set_draft(self):
        self._check_transfer_outgoing()
        picking = self.in_picking_id if self.type == 'incoming' else self.out_picking_id
        picking.action_cancel()
        picking.unlink()
        for line in self.line_ids:
            line.is_available = False
        self.state = 'draft'

    def action_cancel(self):
        if self.state == 'draft':
            self.state = 'cancel'
        elif self.state == 'confirmed':
            picking = self.in_picking_id if self.type == 'incoming' else self.out_picking_id
            picking.action_cancel()
            self.state = 'cancel'

    def delete_duplicate(self):
        for line in self.line_ids:
            transfer_line_id = self.env['stock.transfer.line'].search([('transfer_id', '=', self.id),
                                                                       ('product_id', '=', line.product_id.id),
                                                                       ('is_available', '=', True)], limit=1)
            if transfer_line_id and len(self.line_ids) > 1:
                if self.type == 'incoming':
                    transfer_line_id.in_quantity += line.in_quantity
                else:
                    transfer_line_id.out_quantity += line.out_quantity
                sql = """
                           Delete from stock_transfer_line
                           where id = %d;
                       """
                self._cr.execute(sql % (line.id))
                self._cr.commit()
            else:
                line.is_available = True

    def check_qty(self):
        check = False
        for line in self.line_ids:
            qty = line.in_quantity if self.type == 'incoming' else line.out_quantity
            if qty <= 0:
                check = True
                break
        if check:
            raise UserError(_('You can not transfer with quantity is 0'))

    def _check_transfer_outgoing(self):
        if self.type == 'incoming' and self.is_internal:
            request_id = self.env['stock.request'].sudo().search([('transfer_in_id', '=', self.id)], limit=1)
            if request_id.transfer_out_id.sudo().state != 'done':
                raise UserError(_('Outgoing is incomplete, you cannot perform incoming'))

    def _check_available_quantity(self):
        check_available = False
        for line in self.line_ids:
            total_availability = self.env['stock.quant']._get_available_quantity(line.product_id,
                                                                                 self.warehouse_id.lot_stock_id)
            precision_digits = self.env['decimal.precision'].precision_get('Product Unit of Measure')
            total_availability = float_round(total_availability, precision_digits=precision_digits,
                                             rounding_method='HALF-UP')
            if total_availability >= line.out_quantity:
                line.state = 'available'
                line.available_qty = total_availability
            else:
                line.state = 'not_available'
                line.available_qty = total_availability
                check_available = True
        return check_available

    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        domain = []
        if name:
            domain = [('name', operator, name)]

        access_right = []
        # Check user root and user view all
        if self.env.uid == SUPERUSER_ID or self.env.user.is_system or self.env.user.x_view_all_site:
            return self._search(domain + args + access_right, limit=limit, access_rights_uid=name_get_uid)

        domain_transfer = ['create_uid', '=', self.env.user.id]

        return self._search(domain + args + domain_transfer, limit=limit, access_rights_uid=name_get_uid)

    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
        if self.env.uid == SUPERUSER_ID or self.env.user.is_system or self.env.user.x_view_all_site:
            domain = domain
        else:
            domain_transfer = ['create_uid', '=', self.env.user.id]
            domain.append(domain_transfer)
        return super(StockTransfer, self).search_read(domain, fields, offset, limit, order)
