# -*- coding: utf-8 -*-

from odoo import models, fields, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError, UserError
from datetime import datetime
from odoo.tools.float_utils import float_compare, float_is_zero, float_round


class StockRequest(models.Model):
    _name = 'stock.request'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'date desc'
    _description = 'Stock Request'

    def _domain_warehouse(self):
        warehouse_ids = self.env['stock.warehouse'].sudo().search([('company_id', '=', self.env.user.company_id.id)])
        domain = [('id', 'in', warehouse_ids.ids)]
        return domain

    name = fields.Char('Number', default=lambda self: _('New'))
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id.id)
    date = fields.Date('Date', default=fields.Datetime.now)
    warehouse_id = fields.Many2one('stock.warehouse', string='Source warehouse', tracking=True)
    warehouse_dest_id = fields.Many2one('stock.warehouse', string='Destination warehouse', tracking=True)

    state = fields.Selection([
        ('draft', 'Draft'),
        ('to_approve', 'To Approve'),
        ('approve', 'Approve'),
        ('reject', 'Reject'),
        ('done', 'Done'),
        ('cancel', 'Cancel'),
    ], copy=False, default='draft', tracking=True)
    transfer_out_id = fields.Many2one('stock.transfer', string='Transfer Out')
    transfer_in_id = fields.Many2one('stock.transfer', string='Transfer In')

    reason = fields.Char('Reason Reject', tracking=True)
    line_ids = fields.One2many('stock.request.line', 'request_id', string='Information')

    # def _compute_transfer(self):
    #     for order in self:
    #         order.transfer_count = len(order.stock_transfer_id)

    def action_view_transfer(self):
        '''
        This function returns an action that display existing delivery orders
        of given sales order ids. It can either be a in a list or in a form
        view, if there is only one delivery order to show.
        '''
        action = self.env.ref('ev_stock_transfer.action_stock_transfer_from').sudo().read()[0]

        if self.stock_transfer_id:
            action['domain'] = [('id', '=', self.stock_transfer_id.id)]
            action['views'] = [(self.env.ref('ev_stock_transfer.stock_transfer_from_form_view').id, 'form')]
            action['res_id'] = self.stock_transfer_id.id
        return action

    def action_send(self):
        self.ensure_one()
        if self.state != 'draft':
            return True
        self._create_send_notification()
        # return
        if not self.line_ids:
            raise ValidationError(_("Missing products required!!"))
        if self.warehouse_id.id == self.warehouse_dest_id.id:
            raise ValidationError(_("Please choose 2 different warehouses"))
        self.delete_duplicate()
        self._check_qty()
        self._check_available()
        self.state = 'to_approve'

    def action_approve(self):
        self.ensure_one()
        if self.state != 'to_approve':
            return
        self.create_transfer_out()
        self.create_transfer_in()
        self.state = 'done'

    def action_reject(self):
        self.ensure_one()
        if self.state != 'to_approve':
            return True
        if not self.reason:
            raise UserError(_('You must enter a reason to refuse'))
        self.state = 'reject'

    def action_set_draft(self):
        self.ensure_one()
        if self.state in ('to_approve', 'reject'):
            self.state = 'draft'

    def create_transfer_out(self):
        line_vals = []
        for line in self.line_ids:
            vals = {
                'product_id': line.product_id.id,
                'uom_id': line.uom_id.id,
                'out_quantity': line.qty,
                'price': line.price_unit,
            }
            line_vals.append((0, 0, vals))
        vals = {
            'warehouse_id': self.warehouse_dest_id.id,
            'warehouse_dest_id': self.warehouse_id.id,
            'origin': self.name,
            'type': 'outgoing',
            'state': 'draft',
            'is_internal': True,
            'line_ids': line_vals
        }
        transfer_id = self.env['stock.transfer'].create(vals)
        transfer_id.action_confirm()
        self.transfer_out_id = transfer_id

    def create_transfer_in(self):
        line_vals = []
        for line in self.line_ids:
            vals = {
                'product_id': line.product_id.id,
                'uom_id': line.uom_id.id,
                'in_quantity': line.qty,
                'price': line.price_unit,
            }
            line_vals.append((0, 0, vals))
        vals = {
            'warehouse_id': self.warehouse_dest_id.id,
            'warehouse_dest_id': self.warehouse_id.id,
            'origin': self.name,
            'type': 'incoming',
            'state': 'draft',
            'is_internal': True,
            'line_ids': line_vals
        }
        transfer_id = self.env['stock.transfer'].create(vals)
        transfer_id.action_confirm()
        self.transfer_in_id = transfer_id

    def action_cancel(self):
        if self.state == 'done':
            if self.stock_transfer_id.state == 'draft':
                self.state = 'cancel'
                self.stock_transfer_id.action_cancel()
            else:
                raise UserError(_("Phiểu chuyển kho đang ở trạng thái khác nháp, bạn không thể hủy được!'."))

    def unlink(self):
        for line in self:
            if line.state != 'draft':
                raise UserError(_("You cannot delete record if the state is not 'Draft'."))
        return super(StockRequest, self).unlink()

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('stock.request') or _('New')
        return super(StockRequest, self).create(vals)

    def _create_send_notification(self):
        msg = 'Phiếu yêu cầu: {} được tạo bởi {} đang chờ duyệt'.format(self.name, self.create_uid.name)
        manager_id = self.env.user.company_id.x_manager_id
        channel = self.env['mail.channel'].channel_get([manager_id.partner_id.id])
        channel_id = self.env['mail.channel'].browse(channel["id"])
        channel_id.message_post(
            body=(msg),
            message_type='comment',
            subtype_xmlid='mail.mt_comment',
        )

    def delete_duplicate(self):
        for line in self.line_ids:
            request_line_id = self.env['stock.request.line'].search([('request_id', '=', self.id),
                                                                     ('product_id', '=', line.product_id.id),
                                                                     ('is_available', '=', True)], limit=1)
            if request_line_id and len(self.line_ids) > 1:
                request_line_id.qty += line.qty
                sql = """
                           Delete from stock_request_line
                           where id = %d;
                       """
                self._cr.execute(sql % (line.id))
                self._cr.commit()
            else:
                line.is_available = True

    def _check_qty(self):
        for line in self.line_ids:
            if line.qty <= 0:
                raise ValidationError(_("The number of requests must be greater than 0!!"))
            else:
                uom_qty = float_round(line.qty, precision_rounding=line.uom_id.rounding, rounding_method='HALF-UP')
                precision_digits = self.env['decimal.precision'].precision_get('Product Unit of Measure')
                qty = float_round(line.qty, precision_digits=precision_digits, rounding_method='HALF-UP')
                if float_compare(uom_qty, qty, precision_digits=precision_digits) != 0:
                    raise UserError(_('The quantity done for the product "%s" doesn\'t respect the rounding precision \
                                                              defined on the unit of measure "%s". Please change the quantity done or the \
                                                              rounding precision of your unit of measure.') % (
                        line.product_id.display_name, line.uom_id.name))

    def _check_available(self):
        check_available = 0
        for line in self.line_ids:
            total_availability = self.env['stock.quant'].sudo()._get_available_quantity(line.product_id,
                                                                                        self.warehouse_dest_id.lot_stock_id)
            precision_digits = self.env['decimal.precision'].precision_get('Product Unit of Measure')
            total_availability = float_round(total_availability, precision_digits=precision_digits,
                                             rounding_method='HALF-UP')
            if total_availability >= line.qty:
                line.state = 'available'
                line.available_qty = total_availability
            else:
                line.state = 'not_available'
                line.available_qty = total_availability
                check_available += 1
        if check_available != 0:
            raise UserError(_('Please adjust the output quantity to match the available quantity'))

    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        domain = []
        if name:
            domain = [('name', operator, name)]

        access_right = []
        # Check user root and user view all
        if self.env.uid == SUPERUSER_ID or self.env.user.is_system or self.env.user.x_view_all_site:
            return self._search(domain + args + access_right, limit=limit, access_rights_uid=name_get_uid)

        domain_request = ['create_uid', '=', self.env.user.id]

        return self._search(domain + args + domain_request, limit=limit, access_rights_uid=name_get_uid)

    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
        if self.env.uid == SUPERUSER_ID or self.env.user.is_system or self.env.user.x_view_all_site:
            domain = domain
        else:
            domain_request = ['create_uid', '=', self.env.user.id]
            domain.append(domain_request)
        return super(StockRequest, self).search_read(domain, fields, offset, limit, order)
