import io
import base64

from odoo import models
from odoo.exceptions import ValidationError

from datetime import datetime
from PIL import Image


class ReportInoutTotal(models.AbstractModel):
    _name = 'report.hc_stock_transfer.report_in_out_total_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, lines):
        try:
            for obj in lines:
                format_title = workbook.add_format(
                    {'font_size': 20, 'valign': 'middle', 'bold': False, 'left': True, 'right': True, 'text_wrap': True, 'align': 'center'
                     })
                format_1 = workbook.add_format(
                    {'font_size': 12, 'valign': 'middle', 'bold': True, 'left': True, 'right': True, 'text_wrap': True, 'align': 'center'
                     })
                format_2 = workbook.add_format(
                    {'font_size': 12, 'valign': 'middle', 'bold': False, 'left': True, 'right': True, 'text_wrap': True, 'align': 'right',
                     })
                format_3 = workbook.add_format(
                    {'font_size': 12, 'valign': 'middle', 'bold': False, 'left': True, 'right': True, 'text_wrap': True, 'align': 'center',
                     })
                format_qty = workbook.add_format(
                    {'font_size': 12, 'valign': 'middle', 'bold': False, 'left': True, 'right': True, 'text_wrap': True,
                     'num_format': '#,###.##'
                     })
                format_money = workbook.add_format(
                    {'font_size': 12, 'valign': 'middle', 'bold': False, 'left': False, 'right': True, 'text_wrap': True,
                     'num_format': '#,##0'
                     })

                img_data = base64.b64decode(obj.company_id.logo)
                image = io.BytesIO(img_data)
                bound_width_height = (200, 100)
                image_data = self.get_resized_image_data(image, bound_width_height)
                im = Image.open(image_data)
                im.seek(0)
                sheet = workbook.add_worksheet('Báo cáo vật liệu tổng hợp')
                sheet.set_column(0, 0, 10)
                sheet.set_column(1, 1, 40)
                sheet.set_column(2, 2, 15)
                sheet.set_column(3, 13, 15)
                sheet.set_column(7, 7, 20)

                sheet.merge_range('A1:B3', '')
                sheet.insert_image('A1:B3', 'myimage.png', {'image_data': image_data})

                sheet.merge_range('A4:G4', 'Báo cáo vật liệu tổng hợp', format_title)
                sheet.write('A6', 'STT', format_1)
                sheet.write('B6', 'Tên vật liệu', format_1)
                sheet.write('C6', 'Đơn vị tính', format_1)
                sheet.write('D6', 'Khối lượng nhập', format_1)
                sheet.write('E6', 'Khối lượng xuất', format_1)
                sheet.write('F6', 'Thành tiền nhập', format_1)
                sheet.write('G6', 'Tồn cuối kỳ', format_1)
                sheet.write('H6', 'Thành tiền cuối kỳ', format_1)

                row_count = 7
                stt = 1
                for line in obj.line_ids:
                    sheet.write('A' + str(row_count), stt, format_3)
                    sheet.write('B' + str(row_count), line.product_id.display_name, format_3)
                    sheet.write('C' + str(row_count), line.uom_id.name, format_3)
                    if line.in_quantity % 1 == 0:
                        sheet.write('D' + str(row_count), line.in_quantity, format_money)
                    else:
                        sheet.write('D' + str(row_count), line.in_quantity, format_qty)
                    if line.out_quantity % 1 == 0:
                        sheet.write('E' + str(row_count), line.out_quantity, format_money)
                    else:
                        sheet.write('E' + str(row_count), line.out_quantity, format_qty)

                    sheet.write('F' + str(row_count), line.price_total_out, format_money)

                    if line.qty_ck % 1 == 0:
                        sheet.write('G' + str(row_count), line.qty_ck, format_money)
                    else:
                        sheet.write('G' + str(row_count), line.qty_ck, format_qty)

                    sheet.write('H' + str(row_count), line.price_ck, format_money)
                    row_count += 1
                    stt += 1

                border_format = workbook.add_format({
                    'border': 1,
                    'align': 'left',
                    'font_size': 10
                })

                sheet.conditional_format(0, 0, row_count - 2, 16,
                                         {'type': 'no_blanks', 'format': border_format})
                sheet.conditional_format(0, 0, row_count - 2, 16,
                                         {'type': 'blanks', 'format': border_format})
        except Exception as e:
            raise ValidationError(e)

    def get_resized_image_data(self, file_path, bound_width_height):
        # get the image and resize it
        im = Image.open(file_path)
        im.thumbnail(bound_width_height, Image.ANTIALIAS)  # ANTIALIAS is important if shrinking

        # stuff the image data into a bytestream that excel can read
        im_bytes = io.BytesIO()
        im.save(im_bytes, format='PNG')
        return im_bytes
