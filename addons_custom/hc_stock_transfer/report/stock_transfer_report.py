# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import tools
from odoo import api, fields, models


class StockTransferReport(models.Model):
    _name = 'stock.transfer.report'
    _description = "Stock Transfer Report"
    _rec_name = 'id'
    _auto = False

    id = fields.Integer("", readonly=True)
    site_id = fields.Many2one('construction.site', 'Construction Site', readonly=True)
    transfer_id = fields.Many2one('stock.transfer', 'Stock transfer', readonly=True)
    partner_id = fields.Many2one('res.partner', 'Partner', readonly=True)
    date_done = fields.Datetime('Transfer Date', readonly=True)
    product_id = fields.Many2one('product.product', 'Product', readonly=True)
    uom_id = fields.Many2one('uom.uom', 'Uom', readonly=True)
    in_quantity = fields.Float('In Qty', readonly=True, group_operator="avg")
    out_quantity = fields.Float('Out Qty', readonly=True, group_operator="avg")
    price = fields.Float('Price', readonly=True, group_operator="avg")

    @api.depends('site_id', 'product_id.name')
    def name_get(self):
        res = []
        for report in self:
            name = '%s - %s' % (report.site_id.name, report.product_id.display_name)
            res.append((report.id, name))
        return res

    def _select(self):
        select_str = """
               id as id,
               site_id,
               transfer_id,
               partner_id,
               date_done,
               product_id,
               uom_id,
               sum(in_quantity)  in_quantity,
               sum(out_quantity) out_quantity,
               price
        """

        return select_str

    def _from(self):
        from_str = """
            (select stl.id   as id,
                    sw.x_construction_site_id site_id,
                     st.id                     transfer_id,
                     st.partner_id             partner_id,
                     st.out_date               date_done,
                     stl.product_id            product_id,
                     stl.uom_id                uom_id,
                     stl.out_quantity          out_quantity,
                     0                         in_quantity,
                     stl.price                 price
              from stock_transfer_line stl
                       join stock_transfer st on stl.transfer_id = st.id
                       join stock_warehouse sw on st.warehouse_id = sw.id
                       left join stock_warehouse swd on st.warehouse_dest_id = swd.id
                       left join res_partner rp on st.partner_id = rp.id
        
              where st.state = 'done'
                and st.type = 'outgoing'
        
              union all
        
              select stl.id   as id,
                     sw.x_construction_site_id site_id,
                     st.id                     transfer_id,
                     st.partner_id             partner_id,
                     st.in_date               date_done,
                     stl.product_id            product_id,
                     stl.uom_id                uom_id,
                     0                         out_quantity,
                     in_quantity               in_quantity,
                     stl.price                 price
              from stock_transfer_line stl
                       join stock_transfer st on stl.transfer_id = st.id
                       join stock_warehouse sw on st.warehouse_dest_id = sw.id
                       left join stock_warehouse swd on st.warehouse_id = swd.id
        
                       left join res_partner rp on st.partner_id = rp.id
              where st.state = 'done'
                and st.type = 'incoming') t
        """

        return from_str

    def _group_by(self):
        group_by_str = """
            id,site_id, transfer_id, partner_id, date_done, product_id, uom_id, price
        """

        return group_by_str

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
                            SELECT
                                %s
                            FROM
                                %s
                            GROUP BY
                                %s
            )""" % (self._table, self._select(), self._from(), self._group_by(),))
