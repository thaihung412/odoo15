# -*- coding: utf-8 -*-
{
    'name': "Stock Transfer",
    'summary': """
    This application allows construction site request goods from the warehouse or purchase
    """,

    'description': """
        This application allows construction site request goods from the warehouse or purchase
    """,
    'author': "Công nguyễn",
    'website': "https://www.facebook.com/cn.dinh/",
    'category': 'Stock',
    'version': '0.1',
    'depends': ['base', 'stock', 'hc_construction_site', 'account'],
    'data': [
        'security/ir.model.access.csv',
        'data/ir_sequence_data.xml',
        'views/stock_transfer_incoming_view.xml',
        'views/stock_transfer_outgoing_view.xml',
        'views/stock_warehouse_view.xml',
        'views/stock_request_view.xml',
        'wizard/report_in_out_view.xml',
        'report/report.xml',
        # 'report/stock_transfer_report_view.xml',
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
