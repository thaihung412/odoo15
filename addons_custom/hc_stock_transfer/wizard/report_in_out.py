from odoo import models, fields, _
from odoo.exceptions import ValidationError, UserError


class ReportInOut(models.TransientModel):
    _name = 'report.in.out'

    warehouse_ids = fields.Many2many('stock.warehouse', 'report_in_out_stock_warehouse_rel', 'warehouse_id', 'stock_id', 'Warehouse')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id.id)
    from_date = fields.Date('From date', required=True)
    to_date = fields.Date('To date', required=True)
    line_ids = fields.One2many('report.in.out.line', 'report_id', 'Line')

    type = fields.Selection([('detail', 'Chi tiết'), ('total', 'Tổng hợp')], 'Loại báo cáo')

    def action_generate(self):
        if self.line_ids:
            self.line_ids.unlink()
        query = self._get_query()
        self.env.cr.execute(query)
        transfer_line_ids = self.env.cr.dictfetchall()
        values = []
        for line in transfer_line_ids:
            vals = {
                'report_id': self.id,
                'date': line.get('date_transfer'),
                'product_id': line.get('product_id'),
                'uom_id': line.get('uom_id'),
                'in_quantity': line.get('in_quantity'),
                'out_quantity': line.get('out_quantity'),
                'price_in': line.get('price_in'),
                'price_out': line.get('price_out'),
                'price_total_in': line.get('price_total_in'),
                'price_total_out': line.get('price_total_out'),
                'customer_name': line.get('customer_name'),
                'construction_site_id': line.get('site_id'),
            }
            values.append((0, 0, vals))
        self.line_ids = values

    def action_generate_total(self):
        if self.line_ids:
            self.line_ids.unlink()
        query = self._get_query_total()
        self.env.cr.execute(query)
        transfer_line_ids = self.env.cr.dictfetchall()
        values = []
        for line in transfer_line_ids:
            price_unit = self._get_price_unit(line.get('product_id'))
            vals = {
                'report_id': self.id,
                'product_id': line.get('product_id'),
                'uom_id': line.get('uom_id'),
                'qty_dk': line.get('ton_dk'),
                'in_quantity': line.get('in_quantity'),
                'out_quantity': line.get('out_quantity'),
                'price_total_out': line.get('price_total_out'),
                'qty_ck': line.get('ton_ck'),
                'price_ck': line.get('ton_ck') * price_unit,
            }
            values.append((0, 0, vals))
        self.line_ids = values

    def action_print_excel(self):
        try:
            if self.type == 'detail':
                report_name = 'hc_stock_transfer.report_in_out_xlsx'
            else:
                report_name = 'hc_stock_transfer.report_in_out_total_xlsx'
            action = self.env['ir.actions.report'].search([
                ('model', '=', self._name),
                ('report_name', '=', report_name),
                ('report_type', '=', 'xlsx'),
            ], limit=1)
            if not action:
                raise UserError(_('Report Template not found'))
            context = dict(self.env.context)
            return action.with_context(context).report_action(self)
        except Exception as e:
            raise ValidationError(e)

    def _get_query(self):
        if self.warehouse_ids:
            warehouse_ids = ','.join([str(idd) for idd in self.warehouse_ids.ids])
        else:
            domain = [] if self.env.user.x_view_all_site or self.env.user.is_system else [
                ('x_construction_site_id', 'in', self.env.user.x_construction_site_ids.ids)]
            user_warehouse_ids = self.env['stock.warehouse'].search(domain)
            warehouse_ids = ','.join([str(idd) for idd in user_warehouse_ids.ids]) if user_warehouse_ids else '-1'

        date_from = self.from_date.strftime('%d/%m/%Y')
        date_to = self.to_date.strftime('%d/%m/%Y')

        query = """
            select date_transfer,
                   product_id,
                   uom_id,
                   sum(in_quantity)     in_quantity,
                   sum(out_quantity)    out_quantity,
                   price_in,
                   price_out,
                   sum(price_total_in)  price_total_in,
                   sum(price_total_out) price_total_out,
                   customer_name,
                   site_id
            from (select st.out_date::date                  date_transfer,
                         stl.product_id                     product_id,
                         stl.uom_id                         uom_id,
                         0                                  in_quantity,
                         stl.out_quantity                   out_quantity,
                         0                                  price_in,
                         stl.price_outgoing                 price_out,
                         0                                  price_total_in,
                         stl.out_quantity * stl.price_outgoing as    price_total_out,
                         case
                             when st.is_internal = FALSE then rp.name
                             else swd.name end        as    customer_name,
                         sw.x_construction_site_id          site_id
                  from stock_transfer_line stl
                           join stock_transfer st on stl.transfer_id = st.id
                           join stock_warehouse sw on st.warehouse_id = sw.id
                           left join stock_warehouse swd on st.warehouse_dest_id = swd.id
                           left join res_partner rp on st.partner_id = rp.id
                           join stock_location sl on sw.lot_stock_id = sl.id
            
                  where st.state = 'done'
                    and st.type = 'outgoing'
                    and (st.warehouse_id in (%s) or '0' = '%s')
                    and (st.out_date + INTERVAL '7 hours')::date >= to_date('%s', 'dd/mm/yyyy')
                    and (st.out_date + INTERVAL '7 hours')::date <= to_date('%s', 'dd/mm/yyyy')
            
                  union all
            
                  select st.in_date::date                  date_transfer,
                         stl.product_id                    product_id,
                         stl.uom_id                        uom_id,
                         stl.in_quantity                   in_quantity,
                         0                                 out_quantity,
                         stl.price                         price_in,
                         0                                 price_out,
                         stl.in_quantity * stl.price as    price_total_in,
                         0                                 price_total_out,
                         case
                             when st.is_internal = FALSE then rp.name
                             else swd.name end       as    customer_name,
                         sw.x_construction_site_id         site_id
                  from stock_transfer_line stl
                           join stock_transfer st on stl.transfer_id = st.id
                           join stock_warehouse sw on st.warehouse_dest_id = sw.id
                           left join stock_warehouse swd on st.warehouse_id = swd.id
                           join stock_location sl on sw.lot_stock_id = sl.id
                           left join res_partner rp on st.partner_id = rp.id
                  where st.state = 'done'
                    and st.type = 'incoming'
                    and (st.warehouse_dest_id in (%s) or '0' = '%s')
                    and (st.in_date + INTERVAL '7 hours')::date >= to_date('%s', 'dd/mm/yyyy')
                    and (st.in_date + INTERVAL '7 hours')::date <= to_date('%s', 'dd/mm/yyyy')) t
            group by date_transfer, product_id, uom_id, price_in, price_out, customer_name, site_id
            order by date_transfer, site_id, product_id, uom_id
        """ % (warehouse_ids, warehouse_ids, date_from, date_to, warehouse_ids, warehouse_ids, date_from, date_to)
        return query

    def _get_query_total(self):
        if self.warehouse_ids:
            warehouse_ids = ','.join([str(idd) for idd in self.warehouse_ids.ids])
        else:
            domain = [] if self.env.user.x_view_all_site or self.env.user.is_system else [
                ('x_construction_site_id', 'in', self.env.user.x_construction_site_ids.ids)]
            user_warehouse_ids = self.env['stock.warehouse'].search(domain)
            warehouse_ids = ','.join([str(idd) for idd in user_warehouse_ids.ids]) if user_warehouse_ids else '-1'

        date_from = self.from_date.strftime('%d/%m/%Y')
        date_to = self.to_date.strftime('%d/%m/%Y')

        query = """
            select product_id,
                   uom_id,
                   sum(ton_dk) as ton_dk,
                   sum(int_qty) as in_quantity,
                   sum(out_qty) as out_quantity,
                   sum(price_out) as price_total_out,
                   sum(int_qty) - sum(out_qty) as ton_ck
            from (select product_id, uom_id, sum(ton_dk) as ton_dk, 0 int_qty, 0 out_qty, 0 price_out
                  from (select stl.product_id  product_id,
                               stl.uom_id      uom_id,
                               stl.in_quantity ton_dk
                        from stock_transfer_line stl
                                 join stock_transfer st on stl.transfer_id = st.id
                                 join stock_warehouse sw on st.warehouse_dest_id = sw.id
                                 join stock_location sl on sw.lot_stock_id = sl.id
                        where st.state = 'done'
                          and st.type = 'incoming'
                          and (st.warehouse_dest_id in ({warehouse_ids}) or '0' = '{warehouse_ids}')
                          and (st.in_date + INTERVAL '7 hours')::date < to_date('{from_date}', 'dd/mm/yyyy')
                        union all
                        select stl.product_id    product_id,
                               stl.uom_id        uom_id,
                               -stl.out_quantity ton_dk
                        from stock_transfer_line stl
                                 join stock_transfer st on stl.transfer_id = st.id
                                 join stock_warehouse sw on st.warehouse_id = sw.id
                                 join stock_location sl on sw.lot_stock_id = sl.id
                        where st.state = 'done'
                          and st.type = 'outgoing'
                          and (st.warehouse_id in ({warehouse_ids}) or '0' = '{warehouse_ids}')
                          and (st.out_date + INTERVAL '7 hours')::date < to_date('{from_date}', 'dd/mm/yyyy')) dk
                  group by product_id, uom_id
            
                  union all
                  select stl.product_id  product_id,
                         stl.uom_id      uom_id,
                         0               ton_dk,
                         stl.in_quantity in_qty,
                         0               out_qty,
                         0               pricet_out
                  from stock_transfer_line stl
                           join stock_transfer st on stl.transfer_id = st.id
                           join stock_warehouse sw on st.warehouse_dest_id = sw.id
                           join stock_location sl on sw.lot_stock_id = sl.id
                  where st.state = 'done'
                    and st.type = 'incoming'
                    and (st.warehouse_dest_id in ({warehouse_ids}) or '0' = '{warehouse_ids}')
                    and (st.in_date + INTERVAL '7 hours')::date >= to_date('{from_date}', 'dd/mm/yyyy')
                    and (st.in_date + INTERVAL '7 hours')::date <= to_date('{to_date}', 'dd/mm/yyyy')
                  union all
                  select stl.product_id                  product_id,
                         stl.uom_id                      uom_id,
                         0                               ton_dk,
                         0                               in_qty,
                         stl.out_quantity,
                         stl.out_quantity * stl.price_outgoing as pricet_out
                  from stock_transfer_line stl
                           join stock_transfer st on stl.transfer_id = st.id
                           join stock_warehouse sw on st.warehouse_id = sw.id
                           join stock_location sl on sw.lot_stock_id = sl.id
                  where st.state = 'done'
                    and st.type = 'outgoing'
                    and (st.warehouse_id in ({warehouse_ids}) or '0' = '{warehouse_ids}')
                    and (st.out_date + INTERVAL '7 hours')::date >= to_date('{from_date}', 'dd/mm/yyyy')
                    and (st.out_date + INTERVAL '7 hours')::date <= to_date('{to_date}', 'dd/mm/yyyy')) xnt
            group by product_id, uom_id
            order by product_id, uom_id
        """.format(warehouse_ids=warehouse_ids, from_date=date_from, to_date=date_to)
        return query

    def _get_price_unit(self, product_id):
        date_to = self.to_date.strftime('%d/%m/%Y')
        query = """
            select stl.price price_unit
                from stock_transfer_line stl
                         join stock_transfer st on stl.transfer_id = st.id
                where st.type = 'incoming'
                and product_id = %s
                and (st.in_date + INTERVAL '7 hours')::date <= to_date('%s', 'dd/mm/yyyy')
                order by st.date desc
                limit 1
        """ % (product_id, date_to)
        self.env.cr.execute(query)
        data = self.env.cr.dictfetchone()
        price_unit = data.get('price_unit') if data.get('price_unit') else 0
        return price_unit


class ReportInOutLine(models.TransientModel):
    _name = 'report.in.out.line'

    report_id = fields.Many2one('report.in.out', 'Report ID')
    product_id = fields.Many2one('product.product', 'Product ID')
    uom_id = fields.Many2one('uom.uom', 'Uom ID')
    date = fields.Date('Date')
    # partner_id = fields.Many2one('res.partner', 'Partner Id')
    construction_site_id = fields.Many2one('construction.site', 'Construction Site')
    in_quantity = fields.Float('Qty In', default=0)
    out_quantity = fields.Float('Qty Out', default=0)
    price_in = fields.Float('Price In', default=0)
    price_out = fields.Float('Price Out', default=0)
    price_total_in = fields.Float('Price Total In', default=0)
    price_total_out = fields.Float('Price Total Out', default=0)
    customer_name = fields.Char('Customer name')
    qty_dk = fields.Float('Tồn đầu kỳ', default=0)
    qty_ck = fields.Float('Tồn cuối kỳ', default=0)
    price_ck = fields.Float('Thành tiền cuối kỳ', default=0)
