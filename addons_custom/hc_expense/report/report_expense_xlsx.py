import io
import base64

from odoo import models
from odoo.exceptions import ValidationError

from datetime import datetime
from PIL import Image


class ReportExpenseXLSX(models.AbstractModel):
    _name = 'report.hc_expense.report_expense_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, lines):
        try:
            for obj in lines:
                format_title = workbook.add_format(
                    {'font_size': 20, 'valign': 'middle', 'bold': True, 'left': True, 'right': True, 'text_wrap': True, 'align': 'center'
                     })
                format_1 = workbook.add_format(
                    {'font_size': 12, 'valign': 'middle', 'bold': True, 'left': True, 'right': True, 'text_wrap': True, 'align': 'center'
                     })
                format_2 = workbook.add_format(
                    {'font_size': 12, 'valign': 'middle', 'bold': False, 'left': True, 'right': True, 'text_wrap': True, 'align': 'center',
                     })
                format_3 = workbook.add_format(
                    {'font_size': 12, 'valign': 'middle', 'bold': False, 'left': True, 'right': True, 'text_wrap': True, 'align': 'right',
                     'num_format': '#,###'
                     })

                img_logo = base64.b64decode(self.env.company.logo)
                image_logo = io.BytesIO(img_logo)
                bound_width_height = (200, 100)
                image_data_logo = self.get_resized_image_data(image_logo, bound_width_height)
                im_logo = Image.open(image_data_logo)
                im_logo.seek(0)
                sheet = workbook.add_worksheet('Báo cáo chi phí')
                sheet.set_column(0, 0, 10)
                sheet.set_column(1, 1, 15)
                sheet.set_column(2, 2, 40)
                sheet.set_column(3, 13, 20)

                sheet.merge_range('A1:B3', '')
                sheet.insert_image('A1:B3', 'mylogoimage.png', {'image_data': image_data_logo})

                sheet.merge_range('A4:F4', 'Báo cáo chi phí', format_title)
                sheet.write('A6', 'STT', format_1)
                sheet.write('B6', 'Ngày', format_1)
                sheet.write('C6', 'Nội dung chi phí', format_1)
                sheet.write('D6', 'Yêu cầu', format_1)
                sheet.write('E6', 'Chi', format_1)
                sheet.write('F6', 'Chứng từ kèm theo', format_1)
                sheet.write('G6', 'Người nhận', format_1)
                sheet.write('H6', 'Người chi', format_1)
                sheet.write('I6', 'Mã CP', format_1)
                sheet.write('J6', 'Mã nhỏ', format_1)
                sheet.write('K6', 'Thuộc DA', format_1)
                if obj.construction_site_ids:
                    domain = [('state', '=', 'done'), ('date_done', '>=', obj.from_date), ('date_done', '<=', obj.to_date),
                              ('construction_site_id', 'in', obj.construction_site_ids.ids)]
                else:
                    if self.env.user.x_view_all_site or self.env.user.is_system:
                        domain = [('state', '=', 'done'), ('date_done', '>=', obj.from_date), ('date_done', '<=', obj.to_date)]
                    else:
                        domain = [('state', '=', 'done'), ('date_done', '>=', obj.from_date), ('date_done', '<=', obj.to_date),
                                  ('construction_site_id', 'in', self.env.user.x_construction_site_ids.ids)]
                report_job_ids = self.env['expense.cost'].search(domain)
                row_count = 7
                stt = 1
                for line in report_job_ids:
                    # img_line = base64.b64decode(line.image)
                    # image_line = io.BytesIO(img_line)
                    # bound_width_height = (200, 100)
                    # image_data_line = self.get_resized_image_data(image_line, bound_width_height)
                    # im_line = Image.open(image_data_line)
                    # im_line.seek(0)
                    sheet.write('A' + str(row_count), stt, format_2)
                    sheet.write('B' + str(row_count), datetime.strftime(line.date_done, '%d/%m/%Y'), format_2)
                    sheet.write('C' + str(row_count), line.content, format_2)
                    sheet.write('D' + str(row_count), line.amount, format_3)
                    sheet.write('E' + str(row_count), line.amount_cost, format_3)
                    # sheet.insert_image('F' + str(row_count), line.name + '.png', {'image_data': image_data_line})
                    sheet.write('G' + str(row_count), line.partner_id.name, format_2)
                    sheet.write('H' + str(row_count), '', format_2)
                    sheet.write('I' + str(row_count), line.expense_category_id.code, format_2)
                    sheet.write('J' + str(row_count), line.expense_category_child_id.code, format_2)
                    sheet.write('K' + str(row_count), line.construction_site_id.name, format_2)
                    row_count += 1
                    stt += 1

                border_format = workbook.add_format({
                    'border': 1,
                    'align': 'left',
                    'font_size': 10
                })

                sheet.conditional_format(0, 0, row_count - 2, 11,
                                         {'type': 'no_blanks', 'format': border_format})
                sheet.conditional_format(0, 0, row_count - 2, 11,
                                         {'type': 'blanks', 'format': border_format})
        except Exception as e:
            raise ValidationError(e)

    def get_resized_image_data(self, file_path, bound_width_height):
        # get the image and resize it
        im = Image.open(file_path)
        im.thumbnail(bound_width_height, Image.ANTIALIAS)  # ANTIALIAS is important if shrinking

        # stuff the image data into a bytestream that excel can read
        im_bytes = io.BytesIO()
        im.save(im_bytes, format='PNG')
        return im_bytes
