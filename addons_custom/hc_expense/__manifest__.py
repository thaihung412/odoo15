# -*- coding: utf-8 -*-
{
    'name': "Expense",
    'summary': """
    This application allows Expense
    """,

    'description': """
        This application allows Expense
    """,
    'author': "Công nguyễn",
    'website': "https://www.facebook.com/cn.dinh/",
    'category': '',
    'version': '0.1',
    'depends': ['base', 'mail', 'hc_construction_site', 'hr_expense'],
    'data': [
        'data/ir_sequence_data.xml',
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/expense_category.xml',
        'views/expense_cost.xml',
        'wizard/report_expense_wizard.xml',
        'report/report.xml',
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
