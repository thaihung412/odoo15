from odoo import models, fields, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError, UserError

from datetime import datetime


class ExpenseCost(models.Model):
    _name = 'expense.cost'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Expense Cost'
    _order = 'create_date desc'

    name = fields.Char('Name', default=lambda self: _('New'))
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id, required=True)
    construction_site_id = fields.Many2one('construction.site', 'Construction Site', domain=[('active', '=', True)], required=True)
    expense_category_id = fields.Many2one('expense.category', 'Expense')
    expense_category_child_id = fields.Many2one('expense.category', 'Expense child')
    partner_id = fields.Many2one('res.partner', 'Partner ')
    partner_cost_id = fields.Many2one('res.partner', 'Partner ')
    date = fields.Datetime('Date', default=fields.Datetime.now, required=True)
    date_done = fields.Datetime('Date Done')
    content = fields.Text('Content', required=True)
    image = fields.Image('Image')
    amount = fields.Float('Amount', default=0, digits=(16, 0))
    amount_cost = fields.Float('Amount Cost', default=0, digits=(16, 0))
    reason = fields.Char('Reason')
    state = fields.Selection([
        ('draft', 'Nháp'),
        ('to_approve', 'Chờ duyệt'),
        ('approve', 'Đã duyệt'),
        ('reject', 'Từ chôi'),
        ('done', 'Hoàn thành'),
        ('cancel', 'Hủy'),
    ], copy=False, default='draft', tracking=True)

    def action_send(self):
        self.ensure_one()
        if self.state != 'draft':
            return True
        self._create_send_notification()
        self.state = 'to_approve'

    def action_approve(self):
        self.ensure_one()
        if self.state != 'to_approve':
            return
        self.date_done = datetime.now()
        self.partner_cost_id = self.env.user.partner_id
        self.state = 'done'

    def action_reject(self):
        self.ensure_one()
        if self.state != 'to_approve':
            return True
        if not self.reason:
            raise UserError(_('Ban chưa nhập lý do từ chối'))
        self.date_done = datetime.now()
        self.state = 'reject'

    def _create_send_notification(self):
        msg = 'Phiếu yêu cầu chi phí: {} được tạo bởi {} đang chờ duyệt'.format(self.name, self.create_uid.name)
        manager_id = self.env.user.company_id.x_manager_id
        channel = self.env['mail.channel'].channel_get([manager_id.partner_id.id])
        channel_id = self.env['mail.channel'].browse(channel["id"])
        channel_id.message_post(
            body=(msg),
            message_type='comment',
            subtype_xmlid='mail.mt_comment',
        )

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('expense.cost') or _('New')
        return super(ExpenseCost, self).create(vals)

    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        domain = []
        if name:
            domain = [('name', operator, name)]

        access_right = []
        # Check user root and user view all
        if self.env.uid == SUPERUSER_ID or self.env.user.is_system or self.env.user.x_view_all_site:
            return self._search(domain + args + access_right, limit=limit, access_rights_uid=name_get_uid)

        if self.env.user.has_group('hc_expense.group_expense_manager'):
            request_domain = ['|', ('create_uid', '=', self.env.user.id),
                              ('construction_site_id', 'in', self.env.user.x_construction_site_ids.ids)]
            expense_cost_ids = self.env['expense.cost'].search(request_domain)
            domain_access = ['id', 'in', expense_cost_ids.ids]
        else:
            domain_access = ['create_uid', '=', self.env.user.id]

        return self._search(domain + args + domain_access, limit=limit, access_rights_uid=name_get_uid)

    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
        if self.env.uid == SUPERUSER_ID or self.env.user.is_system or self.env.user.x_view_all_site:
            domain = domain
        else:
            if self.env.user.has_group('hc_expense.group_expense_manager'):
                request_domain = ['|', ('create_uid', '=', self.env.user.id),
                                  ('construction_site_id', 'in', self.env.user.x_construction_site_ids.ids)]
                expense_cost_ids = self.env['expense.cost'].search(request_domain)
                domain_access = ['id', 'in', expense_cost_ids.ids]
            else:
                domain_access = ['create_uid', '=', self.env.user.id]
            domain.append(domain_access)
        return super(ExpenseCost, self).search_read(domain, fields, offset, limit, order)
