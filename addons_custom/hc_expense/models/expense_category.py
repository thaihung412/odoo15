from odoo import models, fields


class ExpenseCategory(models.Model):
    _name = 'expense.category'
    _description = 'Expense Category'
    _order = 'create_date desc'

    name = fields.Char('Name', required=True)
    code = fields.Char('Code', index=True, required=True)
    is_child = fields.Boolean('Child', default=False)
    active = fields.Boolean('Active', default=True)

    _sql_constraints = [
        ('code_uniq', 'unique (code)', 'Mã chi phí đã tồn tại!')
    ]

